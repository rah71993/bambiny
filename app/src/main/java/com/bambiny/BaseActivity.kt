package com.bambiny

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.AlphaAnimation
import androidx.core.content.ContextCompat

/**
 * Created by rahulAndroid on 07/04/21.
 */

open class BaseActivity : Activity() {

    var mProgressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mProgressDialog = ProgressDialog(this)
        mProgressDialog!!.setMessage(resources.getString(R.string.Loading) + "...")
        mProgressDialog!!.setCanceledOnTouchOutside(false)
        mProgressDialog!!.setCancelable(false)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(
                this@BaseActivity,
                R.color.statusColor
            )
        }
    }

    fun goToActivity(classActivity: Class<*>?, bundle: Bundle?) {
        val intent = Intent(this, classActivity)
        if (bundle != null) intent.putExtra(PAYLOAD_BUNDLE, bundle)
        startActivity(intent)
    }

    fun goToActivityWithFinish(classActivity: Class<*>?, bundle: Bundle?) {
        val intent = Intent(this, classActivity)
        if (bundle != null) intent.putExtra(PAYLOAD_BUNDLE, bundle)
        startActivity(intent)
        finish()
    }

    fun clickEffect(v: View) {
        val buttonClick = AlphaAnimation(1f, 0.8f)
        v.startAnimation(buttonClick)
    }

    companion object {
        const val PAYLOAD_BUNDLE = "flag"
    }

}
