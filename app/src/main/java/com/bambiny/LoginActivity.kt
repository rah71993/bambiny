package com.bambiny

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bambiny.databinding.ActivityLoginBinding

class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        initialization()
    }

    private fun initialization() {
        binding.ivLogin.setOnClickListener {
            goToActivity(MainActivity::class.java,null)
        }
        binding.tSignUp.setOnClickListener {
            goToActivity(RegistrationActivity::class.java,null)
        }
    }
}
