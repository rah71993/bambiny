package com.bambiny

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bambiny.adapter.CategoryAdapter
import com.bambiny.adapter.FeaturedItemAdapter
import com.bambiny.databinding.FragmentWelcomeBinding

class WelcomeFragment : Fragment() {
    private lateinit var binding: FragmentWelcomeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_welcome, container, false)
        categories()
        showFeaturedItems()
        return binding.root
    }

    private fun categories() {
        val mLayoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.recycleCategory.layoutManager = mLayoutManager
        binding.recycleCategory.itemAnimator = DefaultItemAnimator()
        val homeAdapter = activity?.let { CategoryAdapter(it) }
        binding.recycleCategory.adapter = homeAdapter
    }


    private fun showFeaturedItems() {
        val popularLayoutManager: RecyclerView.LayoutManager = GridLayoutManager(activity, 1)
        binding.recyclerViewFeaturedItems.layoutManager = popularLayoutManager
        binding.recyclerViewFeaturedItems.itemAnimator = DefaultItemAnimator()
        var featuredItemAdapter = activity?.let { FeaturedItemAdapter(it) }
        binding.recyclerViewFeaturedItems.adapter = featuredItemAdapter
    }
}