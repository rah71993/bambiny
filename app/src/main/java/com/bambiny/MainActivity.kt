package com.bambiny

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.animation.AlphaAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import com.bambiny.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        homeFragment()
        navHeader()

    }

    private fun homeFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.frameHome, WelcomeFragment())
            .commit()
    }

    private fun navHeader() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        binding.relBackHome.setOnClickListener(View.OnClickListener { v ->
            clickEffect(v)
            drawer.openDrawer(GravityCompat.START)
        })
        binding.mainHeader.llSetting.setOnClickListener(View.OnClickListener { v ->
            clickEffect(v)
//            goToActivity(SettingActivity::class.java, null)
            closeHeader()
        })
        binding.mainHeader.llAbout.setOnClickListener(View.OnClickListener { v ->
            clickEffect(v)
//            goToActivity(AboutUsActivity::class.java, null)
            closeHeader()
        })
        binding.mainHeader.tOrderHistory.setOnClickListener(View.OnClickListener { v ->
            clickEffect(v)
            //                if (isLogin()) {
//            goToActivity(OrderHistoryActivity::class.java, null)
            closeHeader()
            //                }
        })
        binding.mainHeader.tTermAndConditions.setOnClickListener(View.OnClickListener {
//            goToActivity(TermAndConditionsActivity::class.java, null)
        })
        binding.mainHeader.llEditProfile.setOnClickListener(View.OnClickListener {
//            goToActivity(EditProfileActivity::class.java, null)
        })
        binding.mainHeader.tPrivacyAndPolicy.setOnClickListener(View.OnClickListener {
//            goToActivity(PrivacyAndPolicyActive::class.java, null)
        })
        binding.mainHeader.llRateUs.setOnClickListener(View.OnClickListener { v ->
            clickEffect(v)
            val rateIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + this@MainActivity.packageName)
            )
            startActivity(rateIntent)
            closeHeader()
        })

    }

    private fun clickEffect(v: View) {
        val buttonClick = AlphaAnimation(1f, 0.8f)
        v.startAnimation(buttonClick)
    }

    private fun goToActivity(classActivity: Class<*>?, bundle: Bundle?) {
        val intent = Intent(this, classActivity)
        if (bundle != null) intent.putExtra(BaseActivity.PAYLOAD_BUNDLE, bundle)
        startActivity(intent)
    }

    private fun closeHeader() {
        binding.drawerLayout.closeDrawer(GravityCompat.START)
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}