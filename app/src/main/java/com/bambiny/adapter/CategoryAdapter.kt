package com.bambiny.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bambiny.R
import com.bambiny.databinding.ListCategoryBinding

class CategoryAdapter(var c: Context) : RecyclerView.Adapter<CategoryAdapter.HolderV>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): HolderV {
        return HolderV(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.list_category, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holderV: HolderV, i: Int) {
//        holderV.binding.tTitle.setText("" + data.get(i).getCategoryName() != null ? data.get(i).getCategoryName() : "");
    }

    override fun getItemCount(): Int {
        return 10
    }

     inner class HolderV(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ListCategoryBinding?

        init {
            binding = DataBindingUtil.bind(itemView)
        }
    }
}