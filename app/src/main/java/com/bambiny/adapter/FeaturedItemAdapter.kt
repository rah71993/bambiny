package com.bambiny.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bambiny.BaseActivity
import com.bambiny.ProductDetailsActivity
import com.bambiny.R
import com.bambiny.databinding.ListFeaturedItemsBinding

class FeaturedItemAdapter(var c: Context) : RecyclerView.Adapter<FeaturedItemAdapter.HolderV>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): HolderV {
        return HolderV(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.list_featured_items, viewGroup, false)
        )
    }

    override fun onBindViewHolder(holderV: HolderV, i: Int) {
        holderV.binding?.llHolder?.setOnClickListener {
            goToActivity(ProductDetailsActivity::class.java, null)
        }
    }

    override fun getItemCount(): Int {
        return 4
    }

    fun goToActivity(classActivity: Class<*>?, bundle: Bundle?) {
        val intent = Intent(c, classActivity)
        if (bundle != null) intent.putExtra(BaseActivity.PAYLOAD_BUNDLE, bundle)
        c.startActivity(intent)
    }

    fun clickEffect(v: View) {
        val buttonClick = AlphaAnimation(1f, 0.8f)
        v.startAnimation(buttonClick)
    }

    inner class HolderV(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ListFeaturedItemsBinding?

        init {
            binding = DataBindingUtil.bind(itemView)
        }
    }
}