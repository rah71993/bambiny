package com.bambiny

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bambiny.databinding.ActivityRegistrationBinding

class RegistrationActivity : BaseActivity() {
    lateinit var binding: ActivityRegistrationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_registration)
        initialization()
    }

    private fun initialization() {
        binding.ivLogin.setOnClickListener {
            goToActivity(MainActivity::class.java, null)
        }
        binding.tSignUp.setOnClickListener {
            goToActivity(RegistrationActivity::class.java, null)
        }
    }
}